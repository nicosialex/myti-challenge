# Myti Practice Test

## Fase 1

La soluzione della Fase 1 è dentro src/main/java/com/gitlab/mytichallenge/Fase1.java
Il codice è commentato spiegando l'approccio adottato

## Fase 2

La classe specificata si trova dentro:
src/main/java/com/gitlab/mytichallenge/entities/Student.java

## Fase 3

Il controller è stato implementato in: src/main/java/com/gitlab/mytichallenge/repositories/StudentController.java
All'interno del file: "MyTi Test.postman_collection.json" c'è la collection postman per invocazione dei due endpoint.

## Starting Info

Per lanciare bisogna avere installato Maven e Java almeno 8.
Eseguire, quindi:

```mvn clean install```

e

```java -jar ./target/myti-challenge-0.0.1-SNAPSHOT.jar```
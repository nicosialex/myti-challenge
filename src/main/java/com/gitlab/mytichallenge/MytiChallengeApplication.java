package com.gitlab.mytichallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * An auto generated SpringBootApplication to handle the server
 */
@SpringBootApplication
public class MytiChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(MytiChallengeApplication.class, args);
	}

}

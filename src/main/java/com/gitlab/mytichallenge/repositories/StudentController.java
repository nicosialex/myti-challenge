package com.gitlab.mytichallenge.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.actuate.endpoint.web.annotation.RestControllerEndpoint;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gitlab.mytichallenge.entities.Student;

/**
 * A simple controller used to handle get and post of Student
 */
@Component
@RestControllerEndpoint(id = "mytichallenge")
public class StudentController {

    private static List<Student> students = new ArrayList<Student>();

    @GetMapping("/student")
    @ResponseBody
    public List<Student> getStudents() {
        // custom logic
        return students;
    }

    @PostMapping("/student")
    public @ResponseBody ResponseEntity<Object> createProduct(@RequestBody Student student) throws Exception {
        if (student.getFirstname() == null || student.getLastname() == null || student.getGrades() == null
                || student.getBirthdate() == null) {
                    return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("The body must contains firstname, lastname, birthdate and grades.");
        }
        if (!student.hasValidBirthDate()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("Error: the date should be passed in the format: dd-mm-yyyy");
        }
        if (student.differenceInYearFromToday() < 0) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("The birthdate couldn't be in the future");
        }
        if (!student.hasValidGradesArray()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("Error: the grades array should contains value separated with ;");
        }
        // custom logic
        students.add(student);
        return new ResponseEntity<Object>(student,HttpStatus.CREATED);
    }
}

package com.gitlab.mytichallenge;

/**
 * A class solution of the Fase 1 of the "Test Ingresso"
 * 
 * @author Alex Nicosia
 */
public class Fase1 {
    
    private static final int LONG_STRING_SAFE_LENGTH = 18;
    private static final long CARRY_OVER_NUMBER = 1000000000000000000L;

    /**
     * The main to be executed to find the wanted number
     * 
     * @param args
     */
    public static void main(String args[]) {
        // define the first two fibonacci sequence numbers
        String a = "0";
        String b = "1";
        // and the length to stop the iteration
        int wantedLength = 1000;
        // loop while the number didn't had the wanted length
        while (b.length() < wantedLength) {
            String next = sumTwoBigNumber(a, b);
            a = b;
            b = next;
        }
        // print the wanted length
        System.out.println("The first Fibonacci Number with length greater than " + wantedLength + ":\n" + b);
   }
    
    /**
     * This method will sum two number passed as String, no matter about their length
     * @param a the first number
     * @param b the second number
     * @return the sum of the above number
     */
    private static String sumTwoBigNumber(String a, String b) {
        // we will split the two integer in multiple long, splitting the Strings in token at most sized as LONG_STRING_SAFE_LENGTH
        // so init two integer as cursor to make the split
        int aCursor = a.length();
        int bCursor = b.length();
        // init the final result to be returned
        String result = "";
        // init a carry over variable
        long carryOver = 0;
        // iterate while the two cursors are greater than 0
        while (aCursor > 0 && bCursor > 0) {
            // set the temp cursor token to 0
            long aToken = 0;
            long bToken = 0;
            // if the cursor is greater than zero, then convert the portion of the number to a Long
            if (aCursor > 0) {
                aToken = Long.valueOf(
                        a.substring(Math.max(0, aCursor - LONG_STRING_SAFE_LENGTH), aCursor));
                aCursor -= LONG_STRING_SAFE_LENGTH;
            }
            // if the cursor is greater than zero, then convert the portion of the number to a Long
            if (bCursor > 0) {
                bToken = Long.valueOf(
                        b.substring(Math.max(0, bCursor - LONG_STRING_SAFE_LENGTH), bCursor));
                bCursor -= LONG_STRING_SAFE_LENGTH;
            }
            // sum the two token with an eventual previous carry over
            long tempResult = aToken + bToken + carryOver;
            // if the new result need a carry over, define it. Otherwise, set it to 0
            if (tempResult > CARRY_OVER_NUMBER) {
                carryOver = tempResult / CARRY_OVER_NUMBER;
                tempResult = tempResult % CARRY_OVER_NUMBER;
            } else {
                carryOver = 0;
            }
            // sum the result with leading 0 so to avoid missing of 0s 
            result = "" + String.format("%0" + LONG_STRING_SAFE_LENGTH + "d", tempResult) + "" + result;
        }

        // Before return of the result, check if there is a carry over to be added 
        if (carryOver != 0) {
            result = "" + carryOver + "" + result;
        }
        
        // remove leading 0s, if any
        String strPattern = "^0+(?!$)";
        result = result.replaceAll(strPattern, "");
        // return the result
        return result;
    }

}

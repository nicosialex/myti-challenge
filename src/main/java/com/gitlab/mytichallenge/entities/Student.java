package com.gitlab.mytichallenge.entities;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.StringTokenizer;

import org.apache.commons.validator.GenericValidator;

/**
 * A class used to represent a student
 * @author Alex Nicosia
 */
public class Student {
    
    // define the attribute of the student
    private String firstname; 
    private String lastname;
    private String birthdate;
    private String grades;

    // getters and setters, used for Spring
    public String getFirstname() {
        return firstname;
    }
    
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGrades() {
        return grades;
    }

    public void setGrades(String grades) {
        this.grades = grades;
    }

    /**
     * A method which will evaluate the age of the Student
     * @return the evaluated date or N.A. if the birthdate has not been defined
     */
    public String getAge() {
        if(!hasValidBirthDate())
            return "Invalid birthdate format";
        int diffInYears = differenceInYearFromToday();
        if (diffInYears < 0)
            return "The birthdate is invalid: it's in the future";
        return "" + diffInYears;
    }
    
    /**
     * Evaluate the difference in Year between today and the birthday
     * @return the difference if the birthdate is valid, Integer.MIN_VALUE otherwise
     */
    public int differenceInYearFromToday() {
        if(!hasValidBirthDate())
            return Integer.MIN_VALUE;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", Locale.ITALY);
        LocalDate ldBirthdate;
        ldBirthdate = LocalDate.parse(birthdate, formatter);
        LocalDate curDate = LocalDate.now();
        return Period.between(ldBirthdate, curDate).getYears();
    }

    /**
     * Evaluate the average grade of a student
     * @return the average grade of the student, or "N.A." if not available
     */
    public String getAvg_grade() {
       try{
            return "" + evaluateAverageGrades();
       }
       catch (Exception e) {
            return "N.A.";
       }
    }

    /**
     * Verify if the birthdate is valid
     * @return
     */
    public boolean hasValidBirthDate() {
        return GenericValidator.isDate(birthdate, "dd-MM-yyyy", true);
    }

    /**
     * Verifies if the grades array is valid
     * @return
     */
    public boolean hasValidGradesArray() {
        if(grades == null)
            return false;
        try{
            evaluateAverageGrades();
        } catch (Exception e){
            return false;
        }
        return true;
    }

    /**
     * Evaluate the average grades from a string containg grades
     * @return
     * @throws NullPointerException
     * @throws NumberFormatException
     */
    private float evaluateAverageGrades() throws NullPointerException, NumberFormatException{
        StringTokenizer tokenizer = new StringTokenizer(grades, ";");
        float average = 0;
        int count = tokenizer.countTokens();
        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            average += Float.parseFloat(token);
        }
        return average / count;
    }
}
